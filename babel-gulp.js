/**
 * Created by gowthaman on 4/24/15.
 */
'use strict';
var gutil = require('gulp-util');
var through = require('through2');
var applySourceMap = require('vinyl-sourcemaps-apply');
var objectAssign = require('object-assign');
var babel = require('babel-core');

module.exports = function (opts) {
    opts = opts || {};

    return through.obj(function (file, enc, cb) {
        if (file.isNull()) {
            cb(null, file);
            return;
        }

        if (file.isStream()) {
            cb(new gutil.PluginError('gulp-babel', 'Streaming not supported'));
            return;
        }

        var paths = file.path.split(".");
        var ext = paths[paths.length - 1]
        if (!opts.extensions || opts.extensions.indexOf(ext) > -1){
            console.log("es6 file -> ", file.path)
            try {
                var fileOpts = objectAssign({}, opts, {
                    filename: file.path,
                    filenameRelative: file.relative,
                    sourceMap: !!file.sourceMap
                });

                var res = babel.transform(file.contents.toString(), fileOpts);

                if (file.sourceMap && res.map) {
                    applySourceMap(file, res.map);
                }

                file.contents = new Buffer(res.code);
                this.push(file);
            } catch (err) {
                //this.emit('error', new gutil.PluginError('gulp-babel', err, {fileName: file.path}));
                console.error("Error in ", file.path)
            }
        }else {
            this.push(file);
        }
        cb();
    });
};
