/**
 * Created by gowthaman on 4/24/15.
 */
var reqCount = 0;
var loading = function () {
    $('#loading').show();
    reqCount++;
};
var doneLoading = function () {
    reqCount--;
    if (reqCount <= 0) $("#loading").hide();
}

angular.module('WazokuDemoApp', ['ngResource', 'ngRoute', 'WazokuDemoAppService', 'mgcrea.ngStrap']).
    config(['$routeProvider', '$locationProvider', '$httpProvider',
        ($routeProvider, $locationProvider, $httpProvider) => {
            $routeProvider.
                when('/challenges', {
                    templateUrl: 'html/challenges.html',
                    controller: 'ChallengesController'
                }).
                when('/conversations', {
                    templateUrl: 'html/conversations.html',
                    controller: 'ConversationsController'
                }).
                when('/conversation/:conversation_id', {
                    templateUrl: 'html/conversation.html',
                    controller: 'ConversationController'
                }).
                when('/challenge/:challenge_id', {
                    templateUrl: 'html/challenge.html',
                    controller: 'ChallengeController'
                }).
                when('/challenge/:challenge_id/stage/:stage_id', {
                    templateUrl: 'html/challenge_stage.html',
                    controller: 'ChallengeStageController'
                }).
                when('/idea/:idea_id', {
                    templateUrl: 'html/idea.html',
                    controller: 'IdeaController'
                }).
                when('/communities', {
                    templateUrl: 'html/communities.html',
                    controller: 'CommunitiesController'
                }).
                when('/community/:community_id', {
                    templateUrl: 'html/community.html',
                    controller: 'CommunityController'
                }).
                otherwise({redirectTo: '/challenges'})

            $httpProvider.interceptors.push(($q)=> {
                return {
                    request: function (config) {
                        loading();
                        return config;
                    },
                    response: function (response) {
                        doneLoading()
                        return response;
                    },
                    responseError: function (rejection) {
                        doneLoading()
                        $("#error-log").prepend(`<li>
                             <p>
                                ${rejection.config.method} - ${rejection.config.url}
                             </p>
                            <small>${rejection.data.description}</small>
                        </li>`)
                        return $q.reject(rejection);
                    }
                }
            });

        }]).
    run(['$rootScope', 'User', '$http', ($rootScope, User, $http)=> {

        $http.defaults.headers.common['X-Auth-Token'] = 'abcd-1234-5678-efgh'
        $http.defaults.headers.common['X-User-ID'] = 'user9'
        $rootScope.current_user = 'user9'
        $http.defaults.headers.common['X-Site-ID'] = 'ku.wazoku.com'
        $rootScope.changeUser = () => {
            console.log("Changing to user ", $rootScope.current_user)
            if ($rootScope.current_user)
                $http.defaults.headers.common['X-User-ID'] = $rootScope.current_user
        }


        $rootScope.userMap = {}
        $rootScope.users = User.query((users)=> {
            users.forEach((u, idx) => {
                $rootScope.userMap[u.id] = u
            })
        })

    }]).
    controller('ChallengesController', ['$scope', '$http', 'Challenge', 'Stage',
        ($scope, $http, Challenge, Stage) => {

            $scope.challenges = Challenge.query()


        }]).
    controller('ChallengeController', ['$scope', '$http', 'Challenge', 'Idea',
        '$routeParams', 'Stage', 'StageForm', '$aside', 'Category', 'Keyword', 'ChallengeStage',
        ($scope, $http, Challenge, Idea, $routeParams, Stage, StageForm, $aside, Category, Keyword, ChallengeStage)=> {
            $scope.challenge = Challenge.get({challenge_id: $routeParams.challenge_id})
            $scope.ideas = Idea.query({challenge_id: $routeParams.challenge_id})
            $scope.categories = Category.query()
            $scope.keywords = Keyword.query()
            $scope.stages = ChallengeStage.query({challenge_id: $routeParams.challenge_id})
            let aside = $aside({scope: $scope, template: 'html/add_idea.html', show: false})
            $scope.addIdea = ()=> {
                let challengeId = $routeParams.challenge_id;

                $scope.idea = {data: {}, challenge_id: challengeId, is_draft: false}

                Stage.query({challenge_id: challengeId}, (stages)=> {
                    let open_stage = stages.filter(s=> {
                        return s['doctype'] == 'OpenStage'
                    })[0]
                    StageForm.get({challenge_id: challengeId, stage_id: open_stage.id}, (questions)=> {
                        $scope.questions = questions;

                        aside.$promise.then(function () {
                            aside.show();
                        })

                    })
                })
            }
            $scope.saveIdea = ()=> {
                Idea.save($scope.idea, (idea)=> {
                    $scope.ideas = Idea.query({challenge_id: $routeParams.challenge_id})
                    aside.$promise.then(function () {
                        aside.hide();
                    })
                })
            }
        }]).
    controller('ChallengeStageController', ['$scope', '$http', 'Challenge', 'ChallengeStage', 'ChallengeStageAdmin', '$routeParams',
        ($scope, $http, Challenge, ChallengeStage, ChallengeStageAdmin, $routeParams)=> {
            $scope.challenge = Challenge.get({challenge_id: $routeParams.challenge_id})
            $scope.stage = ChallengeStage.get({challenge_id: $routeParams.challenge_id, stage_id: $routeParams.stage_id})
            let _load_stage_admins = ()=> {
                $scope.stage_admins = ChallengeStageAdmin.query({challenge_id: $routeParams.challenge_id, stage_id: $routeParams.stage_id})
            }

            _load_stage_admins();
            let q = {challenge_id: $routeParams.challenge_id, stage_id: $routeParams.stage_id};

            $scope.addStageAdmin = (user_id)=> {
                ChallengeStageAdmin.save(q, {user_id: user_id}, ()=> {
                    _load_stage_admins()
                })
            }

            $scope.removeStageAdmin = (user_id)=> {
                q['user_id'] = user_id
                ChallengeStageAdmin.delete(q, ()=> {
                    _load_stage_admins()
                })
            }
        }]).
    controller('IdeaController', ['$scope', '$http', 'Idea', 'Challenge', 'IdeaVote', 'IdeaComment', 'IdeaCommentLike', 'IdeaShare', '$routeParams',
        ($scope, $http, Idea, Challenge, IdeaVote, IdeaComment, IdeaCommentLike, IdeaShare, $routeParams)=> {
            $scope.idea = Idea.get({idea_id: $routeParams.idea_id}, (idea)=> {
                $scope.challenge = Challenge.get({challenge_id: idea.challenge_id})
            })
            $scope.ideaVotes = IdeaVote.query({idea_id: $routeParams.idea_id})
            $scope.ideaShares = IdeaShare.query({idea_id: $routeParams.idea_id})
            let load_idea_comments = ()=> {
                $scope.ideaComments = IdeaComment.query({idea_id: $routeParams.idea_id})
                $scope.c = {
                    response_to_id: $routeParams.idea_id,
                    status: "active",
                    is_deleted: false,
                    comment: "Comment from API ",
                    creator_id: 5,
                    is_advisor_comment: false
                }
            }

            load_idea_comments();
            $scope.deleteComment = (idea_id, comment_id) => {
                IdeaComment.delete({idea_id: idea_id, comment_id: comment_id}, ()=> {
                    load_idea_comments()
                })
            }
            $scope.saveComment = () => {
                IdeaComment.save({idea_id: $routeParams.idea_id}, $scope.c, ()=> {
                    load_idea_comments()
                })
            }

            $scope.upVote = {is_upvote: true}
            $scope.downVote = {is_upvote: false}
        }]).
    controller('ConversationController', ['$scope', '$http', 'Conversation', '$routeParams', 'ConversationAdmin', 'ConversationComment', 'ConversationShare',
        ($scope, $http, Conversation, $routeParams, ConversationAdmin, ConversationComment, ConversationShare)=> {
            var q = {conversation_id: $routeParams.conversation_id};
            $scope.conversation = Conversation.get(q)

            function _load_admins() {
                $scope.conversationAdmins = ConversationAdmin.query(q)
                $scope.a = {}
            }

            function _load_comments() {
                $scope.conversationComments = ConversationComment.query(q)
                $scope.c = {
                    response_to_id: $routeParams.conversation_id,
                    status: "active",
                    is_deleted: false,
                    comment: "Comment from API ",
                    creator_id: 5
                }
            }

            function _load_shares() {
                $scope.conversationShares = ConversationShare.query(q)
                $scope.s = {}
            }

            _load_comments();
            _load_admins();
            _load_shares();

            $scope.saveAdmin = (user_id) => {
                ConversationAdmin.save(q, {user_id: user_id}, ()=> {
                    _load_admins()
                })
            }
            $scope.removeAdmin = (user_id)=> {
                ConversationAdmin.delete({conversation_id: $routeParams.conversation_id, user_id: user_id}, ()=> {
                    _load_admins()
                })
            }
            $scope.unShareConv = (user_id)=> {
                ConversationShare.delete({conversation_id: $routeParams.conversation_id, user_id: user_id}, ()=> {
                    _load_shares()
                })
            }
            $scope.shareConv = (user_id)=> {
                ConversationShare.save(q, {user_id: user_id}, ()=> {
                    _load_shares()
                })
            }
            $scope.deleteConvComment = (comment_id)=> {
                ConversationComment.delete({conversation_id: $routeParams.conversation_id, comment_id: comment_id}, ()=> {
                    _load_comments()
                })
            }

            $scope.saveComment = () => {
                ConversationComment.save(q, $scope.c, ()=> {
                    _load_comments()
                })
            }
        }]).
    controller('ConversationsController', ['$scope', '$http', 'Conversation',
        ($scope, $http, Conversation)=> {
            $scope.conversations = Conversation.query()
        }]).
    controller('CommunitiesController', ['$scope', '$http', 'Community', 'Challenge', 'Conversation',
        ($scope, $http, Community, Challenge, Conversation)=> {
            $scope.communities = Community.query()
        }]).
    controller('CommunityController', ['$scope', '$http', 'Community', 'Challenge', 'Conversation', 'CommunityManager', 'CommunityMember', '$routeParams',
        ($scope, $http, Community, Challenge, Conversation, CommunityManager, CommunityMember, $routeParams)=> {
            var q = {community_id: $routeParams.community_id};
            $scope.community = Community.get(q)
            let _load_managers = () => {
                $scope.managers = CommunityManager.query(q)
            }
            let _load_members = ()=> {
                $scope.members = CommunityMember.query(q)
            }

            _load_managers()
            _load_members()
            $scope.addAdmin = (user_id)=> {
                CommunityManager.save(q, {user_id: user_id}, ()=> {
                    _load_managers()
                })
            }
            $scope.removeAdmin = (user_id)=> {
                CommunityManager.delete({user_id: user_id, community_id: $routeParams.community_id}, ()=> {
                    _load_managers()
                })
            }
            $scope.addMember = (user_id)=> {
                CommunityMember.save(q, {user_id: user_id}, ()=> {
                    _load_members()
                })
            }
            $scope.removeMember = (user_id)=> {
                CommunityMember.delete({user_id: user_id, community_id: $routeParams.community_id}, ()=> {
                    _load_members()
                })
            }
        }]);