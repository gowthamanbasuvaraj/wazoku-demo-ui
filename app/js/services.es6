/**
 * Created by gowthaman on 4/24/15.
 */
var API_SERVER = ""
angular.module('WazokuDemoAppService', ['ngResource']).
    factory('Community', ($resource) => {
        return $resource(API_SERVER + '/api/v1/community/:community_id')
    }).
    factory('CommunityManager', ($resource) => {
        return $resource(API_SERVER + '/api/v1/community/:community_id/manager')
    }).
    factory('CommunityMember', ($resource) => {
        return $resource(API_SERVER + '/api/v1/community/:community_id/member')
    }).
    factory('User', ($resource) => {
        return $resource(API_SERVER + '/api/v1/user')
    }).
    factory('Category', ($resource) => {
        return $resource(API_SERVER + '/api/v1/category')
    }).
    factory('Keyword', ($resource) => {
        return $resource(API_SERVER + '/api/v1/keyword')
    }).
    factory('Challenge', ($resource) => {
        return $resource(API_SERVER + '/api/v1/challenge/:challenge_id')
    }).
    factory('Stage', ($resource) => {
        return $resource(API_SERVER + '/api/v1/challenge/:challenge_id/stage')
    }).
    factory('StageForm', ($resource) => {
        return $resource(API_SERVER + '/api/v1/challenge/:challenge_id/stage/:stage_id/form')
    }).
    factory('ChallengeFollow', ($resource) => {
        return $resource(API_SERVER + '/api/v1/challenge/:challenge_id/follow')
    }).
    factory('ChallengeShare', ($resource) => {
        return $resource(API_SERVER + '/api/v1/challenge/:challenge_id/share')
    }).
    factory('ChallengeStage', ($resource) => {
        return $resource(API_SERVER + '/api/v1/challenge/:challenge_id/stage/:stage_id')
    }).
    factory('ChallengeStageAdmin', ($resource) => {
        return $resource(API_SERVER + '/api/v1/challenge/:challenge_id/stage/:stage_id/admin')
    }).
    factory('Idea', ($resource) => {
        return $resource(API_SERVER + '/api/v1/idea/:idea_id')
    }).
    factory('IdeaVote', ($resource) => {
        return $resource(API_SERVER + '/api/v1/idea/:idea_id/vote')
    }).
    factory('IdeaShare', ($resource) => {
        return $resource(API_SERVER + '/api/v1/idea/:idea_id/share')
    }).
    factory('IdeaComment', ($resource) => {
        return $resource(API_SERVER + '/api/v1/idea/:idea_id/comment')
    }).
    factory('IdeaCommentLike', ($resource) => {
        return $resource(API_SERVER + '/api/v1/idea/:idea_id/comment/:comment_id/like')
    }).
    factory('Conversation', ($resource) => {
        return $resource(API_SERVER + '/api/v1/conversation/:conversation_id')
    }).
    factory('ConversationAdmin', ($resource) => {
        return $resource(API_SERVER + '/api/v1/conversation/:conversation_id/comment_moderator')
    }).
    factory('ConversationShare', ($resource) => {
        return $resource(API_SERVER + '/api/v1/conversation/:conversation_id/share')
    }).
    factory('ConversationComment', ($resource) => {
        return $resource(API_SERVER + '/api/v1/conversation/:conversation_id/comment')
    }).
    factory('ConversationCommentLike', ($resource) => {
        return $resource(API_SERVER + '/api/v1/conversation/:conversation_id/comment/:comment_id/like')
    })
