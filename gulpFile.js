/**
 * Created by gowthaman on 4/24/15.
 */
var gulp = require('gulp');
var concat = require('gulp-concat-util');
var babel = require('./babel-gulp');

var paths = {
    scripts: [
        './bower_components/jquery/dist/jquery.min.js',
        './bower_components/angular/angular.js',
        './bower_components/angular-route/angular-route.min.js',
        './bower_components/angular-i18n/angular-locale_en-in.js',
        './bower_components/angular-resource/angular-resource.min.js',
        './bower_components/angular-sanitize/angular-sanitize.min.js',
        './bower_components/angular-animate/angular-animate.min.js',
        './bower_components/angular-once/once.js',
        './bower_components/angular-strap/dist/angular-strap.min.js',
        './bower_components/angular-strap/src/helpers/date-parser.js',
        './bower_components/angular-strap/dist/angular-strap.tpl.min.js',
        './bower_components/moment/min/moment-with-locales.min.js',
        './bower_components/ui-router/release/angular-ui-router.min.js',
        /*Our app Files */
        './app/js/*.es6'
    ],
    css: [
        './bower_components/bootstrap/dist/css/bootstrap.min.css',
        './bower_components/bootstrap/dist/css/bootstrap-theme.min.css',
        './bower_components/fontawesome/css/font-awesome.min.css',
        './bower_components/fontawesome-actions/dist/css/font-awesome.css',
        /*Our styles*/
        './app/css/*.css'
    ],
    fonts: [
        './bower_components/bootstrap/dist/fonts/*',
        './bower_components/fontawesome-actions/dist/fonts/*'
    ],
    html: [
        'app/html/{,*/}*.html',
        'app/index.html'
    ],
    images: [
        'app/images/*'
    ]
}


gulp.task('scripts', function () {
    gulp.src(paths.scripts)
        .pipe(babel({extensions: ['es6']}))
        .pipe(concat('all.js'))
        .pipe(gulp.dest('./dist/js'))


});

gulp.task('css', function () {

    gulp.src(paths.css)
        .pipe(concat('all.css'))
        .pipe(gulp.dest('./dist/css'))


});

gulp.task('fonts', function () {
    gulp.src(paths.fonts)
        .pipe(gulp.dest('./dist/fonts'))
    gulp.src(paths.images)
        .pipe(gulp.dest('./dist/images'))
});


gulp.task('htmls', function () {
    gulp.src('app/html/{,*/}*.html')
        .pipe(gulp.dest('./dist/html'));

    gulp.src('app/index.html')
        .pipe(gulp.dest('./dist'))
});

// Rerun the task when a file changes
gulp.task('watch', function () {
    gulp.watch(paths.scripts, ['scripts']);
    gulp.watch(paths.css, ['css']);
    gulp.watch(paths.html, ['htmls']);
});

// The default task (called when you run `gulp` from cli)
gulp.task('default', ['watch', 'scripts', 'css', 'fonts', 'htmls']);